package com.example.myapplication;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class DefaultService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int state = super.onStartCommand(intent, flags, startId);
        Log.e("testService","onStart");
        try {
            Thread.sleep(25000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.e("testService","sleep 25000 done!");
        return state;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
