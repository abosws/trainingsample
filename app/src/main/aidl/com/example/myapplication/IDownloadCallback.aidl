package com.example.myapplication;
interface IDownloadCallback {
    void updatePercentage(int percentage);
}
