// IRemoteInterface.aidl
package com.example.myapplication;
import com.example.myapplication.IDownloadCallback;
interface IRemoteInterface {
    void registerCallback(IDownloadCallback callback);
    boolean downloadFile(String url);
}
